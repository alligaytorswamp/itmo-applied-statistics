#v1.0
#Python 3.7.4
#GNU GPLv3
#По вопросам t.me/chernyaknikita


from math import *
from decimal import *


#Вычисление математического ожидания
def expValue(values, probs):
	return sum([values[j] * probs[j] for j in range(len(values))])


#Вычисление дисперсии
def dispersion(xExpValue, values, probs):
	return expValue([(j - xExpValue) ** 2 for j in values], probs)


#Вывод данных
def output(table, xExpValue, xDispersion):
	for j in range(len(table[0])):
		print('Вероятность P (' + table[2] + ' = ' + str(table[0][j]) + ') = ' + str(table[1][j]))
	print('Математическое ожидание E(' + table[2] + ') = ' + str(xExpValue))
	print('Дисперсия D(' + table[2] + ') = ' + str(xDispersion) + '\n')


eTable = [[int(input('Введите значение e' + str(j + 1) + ': ')) for j in range(4)], [0 for j in range(4)], 'e']
nTable = [[int(input('Введите значение n' + str(j + 1) + ': ')) for j in range(2)], [0 for j in range(2)], 'n']
t = []
for i in range(2):
	for j in range(4):
		tmp = float(input('Введите значение ячейки с парой индексов ' + str(i + 1) + ',' + str(j + 1) + ': '))
		t.append(tmp)
		eTable[1][j] += tmp
		nTable[1][i] += tmp


print('_' * 50)		
if sum(eTable[1]) != 1 or sum(nTable[1]) != 1:
	print('Вы ошиблись при вводе данных :с')
else:
	eExpValue = expValue(eTable[0], eTable[1])
	eDispersion = dispersion(eExpValue, eTable[0], eTable[1])
	output(eTable, eExpValue, eDispersion)


	nExpValue = expValue(nTable[0], nTable[1])
	nDispersion = dispersion(nExpValue, nTable[0], nTable[1])
	output(nTable, nExpValue, nDispersion)


	cov = expValue([nTable[0][j // 4] * eTable[0][j % 4] for j in range(8)], t) - eExpValue * nExpValue
	print('Ковариация cov(' + eTable[2] + ', ' + nTable[2] + ') = ' + str(cov))
	print('Коэффициент корреляции p(' + eTable[2] + ', ' + nTable[2] + ') = ' + str(Decimal(cov / sqrt(eDispersion) / sqrt(nDispersion)).quantize(Decimal('0.001'))))