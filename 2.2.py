#v1.0
#Python 3.7.4
#GNU GPLv3
#По вопросам t.me/chernyaknikita


from math import *
from decimal import *
getcontext().prec = 20


#Ввод данных
n, p = Decimal(int(input('Количество экспериментов: '))), Decimal(float(input('Вероятность успеха: ')))
a = []
for j in range(int(input('Нижний порог количества успехов: ')), int(input('Верхний порог количества успехов: ')) + 1):
	a.append(Decimal(j))


#Формула Бернулли
def pBernoulli(a, p, n):
	output = Decimal(0)
	fn = factorial(n)
	for k in a:
		output += fn * p ** k * (1 - p) ** (n - k) / factorial(k) / factorial(n - k)
	print('Вероятность по формуле Бернулли: ', output.quantize(Decimal('0.001')))
	print()


#Теорема Пуассона
def pPoisson(a, p, n):
	output = Decimal(0)
	l = p * n
	for k in a:
		output += l ** k * Decimal(- l).exp() / factorial(k)
	output = output.quantize(Decimal('0.001'))
	print('Вероятность по теореме Пуассона: ', output)

	leftLimitPoint = output - min(p, l * p)
	if leftLimitPoint < 0:
		print('Левая граница интервала:', 0)
	else:
		print('Левая граница интервала:', leftLimitPoint)

	rightLimitPoint = output + min(p, l * p)
	if rightLimitPoint > 1:
		print('Правая граница интервала:', 1)
	else:
		print('Правая граница интервала:', rightLimitPoint)

	print()


#Интегральная теорема Муавра-Лапласа
def pDeMoivreLaplace(a, p, n):
	print('X(B): ', Decimal((a[len(a) - 1] - n * p) / Decimal(n * p * (1 - p)).sqrt()).quantize(Decimal('0.01')))
	print('X(A): ', Decimal((a[0] - n * p) / Decimal(n * p * (1 - p)).sqrt()).quantize(Decimal('0.01')))
	print('Вычислите Ф(X(B)) - Ф(X(A)), чтобы получить вероятность по интегральной теореме Муавра-Лапласа.\nФ(X) берутся из таблицы: http://matecos.ru/formuly/formuly-i-tablitsy/tablitsa-laplasa.html\nP.S. Ф(-X) = -Ф(X)')
	print('Погрешность: ', Decimal(1 / p / (1 - p) / n.sqrt()).quantize(Decimal('0.001')))


print('_' * 50)
pBernoulli(a, p, n)
pPoisson(a, p, n)
pDeMoivreLaplace(a, p, n)